package com.example.kotlin9

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var surnameEditText: EditText
    private lateinit var nameEditText: EditText
    private lateinit var patronymicEditText: EditText
    private lateinit var ageEditText: EditText
    private lateinit var hobbyEditText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

// Получение ссылок на элементы интерфейса
        surnameEditText = findViewById(R.id.surname_edit_text)
        nameEditText = findViewById(R.id.name_edit_text)
        patronymicEditText = findViewById(R.id.patronymic_edit_text)
        ageEditText = findViewById(R.id.age_edit_text)
        hobbyEditText = findViewById(R.id.hobby_edit_text)

// Обработка нажатия на кнопку
        findViewById<Button>(R.id.submit_button).setOnClickListener {
// Переход на InfoActivity с передачей данных
            val intent = Intent(this, InfoActivity::class.java)
            intent.putExtra("surname", surnameEditText.text.toString())
            intent.putExtra("name", nameEditText.text.toString())
            intent.putExtra("patronymic", patronymicEditText.text.toString())
            intent.putExtra("age", ageEditText.text.toString().toInt())
            intent.putExtra("hobby", hobbyEditText.text.toString())
            startActivity(intent)
        }
    }
}