package com.example.kotlin9

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import java.util.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

// Создание layout с картинкой-заставкой
        val layout = FrameLayout(this)
        val imageView = ImageView(this)
        imageView.setImageResource(R.drawable.splash_image)
        layout.addView(imageView, FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT))

        setContentView(layout)

// Задержка на 3 секунды
        Handler().postDelayed({
// Переход на MainActivity
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }, 3000)
    }
}