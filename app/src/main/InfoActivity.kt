package com.example.kotlin9

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import java.util.*

class InfoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

// Получение переданных данных
        val surname = intent.getStringExtra("surname")
        val name = intent.getStringExtra("name")
        val patronymic = intent.getStringExtra("patronymic")
        val age = intent.getIntExtra("age", 0)
        val hobby = intent.getStringExtra("hobby")

// Формирование текста в зависимости от возраста
        val text = when (age) {
            in 0..17 -> "Вы еще не совершеннолетний(-я) $surname $name $patronymic. $hobby"
            in 18..59 -> "Привет, $name $surname! Ваш возраст $age лет. Расскажите больше о своем хобби: $hobby"
            else -> "Здравствуйте, уважаемый(-ая) $surname $name